// Sample program to show how to show you how to briefly work
// with the sql package.
package main

import (
	"database/sql"
	"log"

	_ "goinaction/chapter3/dbdriver/postgres"
)

// main is the entry point for the application.
func main() {
	_, err := sql.Open("postgres", "mydb")
	if err != nil {
		log.Println("xxxx")
	}
}
